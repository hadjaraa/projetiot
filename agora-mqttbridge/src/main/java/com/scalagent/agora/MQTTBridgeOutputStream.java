package com.scalagent.agora;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

public class MQTTBridgeOutputStream implements MqttCallback {
    private final static Logger logger = Logger.getLogger(MQTTBridgeOutputStream.class.getName());

    private String topic;
    private int qos;
    private MqttConnectOptions mqttOptions;
    private static final int maxClientIdLength = 23;

    MqttClient client;

    /**
     * Creates an output stream.
     *
     * @param mqtturi
     * @param loginAgora
     * @param qos
     * @throws MqttException
     */
    public MQTTBridgeOutputStream(String mqtturi,
                                  String apikey,
                                  String loginAgora,
                                  int qos) throws MqttException {
        String mqtturi1 = mqtturi;
        this.qos = qos;
        String clientId = "A " + loginAgora;
        if (clientId.length() > maxClientIdLength)
            clientId = clientId.substring(0, maxClientIdLength - 1);

        this.topic = apikey.concat("/streams");

        logger.log(Level.INFO, "MQTTBridgeOutputStream: Topic: " + topic);

        // Creates MQTT connection to Agora broker
        client = new MqttClient(mqtturi, clientId);
        client.setCallback(this);

        mqttOptions = new MqttConnectOptions();
        if ((apikey != null) && (apikey.length() > 0))
            mqttOptions.setUserName(apikey);
        mqttOptions.setCleanSession(true);
        mqttOptions.setKeepAliveInterval(60);

    }

    public void start() throws MqttException {
        // Connect to the server
        client.connect(mqttOptions);
        logger.log(Level.INFO, "MQTTBridgeOutputStream.start: connected to IotAgora.");
    }

    public void publish(String deviceID, String dataJson) throws MqttException {
        StringBuffer message = new StringBuffer();
        message.append("{\n" +
                "\"protocol\": \"v2\",\n" +
                "\"device\":\"").append(deviceID).append("\",\n")
                .append("\"at\": \"now\",\n" +
                        "\"data\": ").append(dataJson).append("}");

        logger.log(Level.INFO, "MQTTBridgeOutputStream.publish: " + (client.isConnected() ? "" : "NOT")
                + " connected. Publishing message: " + message.toString());
        client.publish(topic, message.toString().getBytes(), qos, false);
    }

    public void connectionLost(Throwable cause) {
        logger.log(Level.WARNING, "MQTTBridgeOutputStream.connectionLost: disconnected from IotAgora.", cause);
        try {
            client.connect(mqttOptions);
            logger.log(Level.INFO, "MQTTBridgeOutputStream.connectionLost: re-connected to IotAgora.");
        } catch (MqttException e) {
            logger.log(Level.SEVERE, "Could not reconnect to IotAgora: " + e.getMessage());
            System.exit(-1);
        }
    }

    public void messageArrived(String topic, MqttMessage message) {
        // Should never happened, logs a message
        logger.log(Level.WARNING, "MQTTBridgeOutputStream: Receives message from " + topic);
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        // TODO Auto-generated method stub
    }
}
