package com.scalagent.agora;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MQTTBridge {
    private final static Logger logger = Logger.getLogger(MQTTBridge.class.getName());

    // URI of JoramMQ MQTT broker (Where the NodeMCU messages are sent)
    private final static String mqtturi1 = "tcp://152.77.47.50:6883";
    private final static String username = "etud11";      // JoramMQ
    private final static String password = "etu11ETU";

    // URI of IoTAgora MQTT broker
    private final static String mqtturi2 = "tcp://mqtt.iotagora.net:1883";
    private static String apikey = null; //"57c067729b2839085d2632f26b139100ace8569c0a5bc03a97be534cb0f59ed4";
    private static String loginAgora = null; //"etud11";
    private final static int qos = 0;

    private static MQTTBridgeInputStream istream;
    private static MQTTBridgeOutputStream ostream;

    public static void main(String[] args) throws Exception {
        apikey = System.getProperty("apikey");
        loginAgora = System.getProperty("login");
        if ((apikey == null) || (loginAgora == null)) {
            if (apikey == null)
                System.err.println("You must define \"apikey\" property");
            if (loginAgora == null)
                System.err.println("You must define \"login\" property");
            System.exit(-1);
        }

        logger.log(Level.INFO, "MQTTBridge: apikey=" + apikey + ", loginAgora=" + loginAgora);

        ostream = new MQTTBridgeOutputStream(mqtturi2, apikey, loginAgora, qos);
        istream = new MQTTBridgeInputStream(mqtturi1, username, password, loginAgora, qos, ostream);

        ostream.start();
        istream.start();
        // Pour tester:  commenter istream.start()  et  decommenter istream.messageArrived()
        // istream.messageArrived("Bruno_Vernay/data/test/moisture", new MqttMessage(("712").getBytes()));
        // System.exit(0);
    }
}
