package com.scalagent.agora;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MQTTBridgeInputStream implements MqttCallback {
    private final static Logger logger = Logger.getLogger(MQTTBridgeInputStream.class.getName());
    private static final int maxClientIdLength = 23;
    private String username, password;
    private String loginAgora;
    private int qos;
    private MqttClient client;

    private MQTTBridgeOutputStream ostream;

    /**
     * Creates an input stream.
     *
     * @param mqtturi
     * @param username
     * @param password
     * @param loginAgora
     * @param qos
     * @throws MqttException
     */
    public MQTTBridgeInputStream(String mqtturi,
                                 String username, String password,
                                 String loginAgora,
                                 int qos,
                                 MQTTBridgeOutputStream ostream) throws MqttException {
        String mqtturi1 = mqtturi;
        this.username = username;
        this.password = password;
        this.loginAgora = loginAgora;
        String clientId = "J " + loginAgora;
        if (clientId.length() > maxClientIdLength)
            clientId = clientId.substring(0, maxClientIdLength - 1);
        this.qos = qos;
        this.ostream = ostream;

        logger.log(Level.INFO, "MQTTBridgeInputStream: UserName: " + username);

        client = new MqttClient(mqtturi, clientId);
        client.setCallback(this);
    }

    public void start() throws MqttException {
        // Connect to the server
        MqttConnectOptions options = new MqttConnectOptions();
        if ((username != null) && (username.length() > 0))
            options.setUserName(username);
        if ((password != null) && (password.length() > 0))
            options.setPassword(password.toCharArray());
        options.setCleanSession(true);
        options.setKeepAliveInterval(60);

        client.connect(options);
        logger.log(Level.INFO, "MQTTBridgeInputStream.start: subscribe to " + loginAgora + "/data/#");
        client.subscribe(loginAgora + "/data/#", qos);
    }

    public void connectionLost(Throwable cause) {
        // TODO: Try to reconnect
        logger.log(Level.SEVERE, "MQTTBridgeInputStream.connectionLost: disconnected from JoramMQ.", cause);
        try {
            start();;
            logger.log(Level.INFO, "MQTTBridgeOutputStream.connectionLost: re-connected to IotAgora.");
        } catch (MqttException e) {
            logger.log(Level.SEVERE, "Could not reconnect to IotAgora: " + e.getMessage());
            System.exit(-1);
        }
    }

    public void messageArrived(String topic, MqttMessage message) {
        logger.log(Level.INFO, "MQTTBridgeInputStream.messageArrived: " + topic + ", " + message.toString());

        // Source topic is "etud##/data/nodemcuXX/[dh12|sht30]/[temperature|humidity]
        String[] topics = topic.split("/");

        if(topics.length >= 4 ) {
          if ("data".equals(topics[1])) {
              String deviceID = topics[2] + "@Bruno_Vernay." + loginAgora;
              String sensor = topics[3];

              String value = new String(message.getPayload());

              StringBuffer dataJson = new StringBuffer();
              dataJson.append("{\"").append(sensor).append("\":").append(value).append("}\n");
              try {
                  ostream.publish(deviceID, dataJson.toString());
              } catch (Exception exc) {
                  logger.log(Level.SEVERE, "MQTTBridgeInputStream.messageArrived: ", exc);
              }
          }
        }
         else {
            logger.log(Level.WARNING, "Only \"data\" is implemented.");
            // TODO: Publish to AgoraIoT status topic
        }
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        // TODO Auto-generated method stub
    }
}
